Team details:
Rajesh Golani (rgolani)
Devesh Bajpai (dbajpai)

Deliverable folder
cd /home/nlclient31/Deliverables

Deliverables details:
Item                Purpose
slides              Slide deck for presentation 
screenshots         provides screenshot for all tasks done
report              Contains detailed report
libs                Contains cross compiled shared libraries
ko                  Contained generated kernel modules required for nftables
kernel              Contains tarball for patched goldfish kernel 3.10
nft					generated nft executable for userspace
bzImage				Patched goldfish kernel 3.10 image

Main Repository can be found on GitHub: 
https://bitbucket.org/rajesh_golani/cse508_nftables_porting

Cross compiled Userspace libraries
In folder /home/nlclient31/Deliverables/lib

Kernel patches
Patch-1 : http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/patch/?id=96518518cc417bb0a8c80b9fb736202e28acdf96
Patch-2 : http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/patch/?id=f59cb0453cd885736daa11ae2445982c5ab2fc83
Patch-3 : http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/patch/?id=795aa6ef6a1aba99050735eadd0c2341b789b53b
patch-4 :http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=20a69341f2d00cd042e81c82289fba8a13c05a25

Generated kernel objects
In folder /home/nlclient31/Deliverables/ko

Buliding kernel:
1. Extract kernel/goldfish_3.10.tar.gz.
2. Go to goldfish_3.10 directory
	cd goldfish_3.10
3. Execute following commands
	make clean
	make i386_emu_defconfig
	make menuconfig 
		*NOTE: In this check all the required nftables options in netfilters as modules
	make ARCH=x86 CC="${CROSS_COMPILE}gcc -mno-android"
		*NOTE: $CROSS_COMPILE is path to prebuilt toolchain. On our nlclient31 $CROSS_COMPILE is set to : /home/nlclient31/goldfish/toolchain/prebuilt/linux-x86/toolchain/i686-android-linux-4.4.3/bin/i686-android-linux-

After this, you should have kernel image in arch/x86/boot/bzImage and all the required ko files. All the applied patches are also included in tarball. Patches are already applied. Don't apply these again. 

Instructions to set up nftables from Emulator: (Please let us know if you face any problems with following steps)
VM: 
FYI: Emulator on VM is painfully slow.
**IMP: Please run following command to load all the environment variables.
1. source /home/nlclient31/.bashrc
2. Go into Deliverables folder
	cd /home/nlclient31/Deliverables
	
--->From here I'm assuming you are in /home/nlclient31/Deliverables folder.

Emulator: 
1. Boot up emulator with kernel image ./bzImage provided in this directory. You can use following command to do this.
 emulator -avd final1 -kernel bzImage

2. Use adb devices to check if device is online.
 adb devices

Userspace and kernel: (Here we are explaining how we did it. There can be many other ways of doing the same thing)
1. Push libraries from ./lib folder to emulator.
  adb push libs/ /data/lib/

2. Push ./nft executable to emulator.
  adb push nft /data/

3. Push kernel objects from ./ko directory to emulator.
  adb push ko/ /data/ko/

4. Go to adb shell
 adb shell

4. cd to /data/ko directory
 cd /data/ko

6. There is one script in ./ko folder called insmod_ko to insmod kos in right order.
 chmod 700 insmod_ko
 ./insmod_ko
 
7. Set up LD_LIBRARY_PATH
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/data/lib/
 
8. Go to /data directory. Give executable permission to nft
 cd /data
 chmod 700 nft

9. Run the nft in interactive mode using following command
 ./nft -i

That's it.

Testing: (User screenshot provided in to get cleat idea)
root # ./nft -i
nft > add table ip filter
nft > add chain ip filter input { type filter hook input priority 0; }
nft > list table filter
list table filter
table ip filter {
		chain input {
	}
}
nft > add rule filter input ip daddr 127.0.0.1 counter
nft > list table filter
list table filter
table ip filter {
		chain input {
			ip daddr localhost counter packets 0 bytes 0
		}
}
nft > quit

root # ping 127.0.0.1
PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.120 ms
64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.517 ms
root # ./nft -i

nft > list table filter
list table filter
table ip filter {
		chain input {
			ip daddr localhost counter packets 10 bytes 840
	}
}
nft > add rule filter input ip daddr 127.0.0.1 drop
nft > list table filter
list table filter
table ip filter {
	chain input {
					ip daddr localhost counter packets 10 bytes 840
					ip daddr localhost drop
	}
}
nft > quit

root # ping -w 5 127.0.0.1
PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
--- 127.0.0.1 ping statistics ---
6 packets transmitted, 0 received, 100% packet loss, time 5000ms
