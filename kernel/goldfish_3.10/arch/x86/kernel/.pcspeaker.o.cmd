cmd_arch/x86/kernel/pcspeaker.o := /home/nlclient31/goldfish/toolchain/prebuilt/linux-x86/toolchain/i686-android-linux-4.4.3/bin/i686-android-linux-gcc -mno-android -Wp,-MD,arch/x86/kernel/.pcspeaker.o.d  -nostdinc -isystem /home/nlclient31/goldfish/toolchain/prebuilt/linux-x86/toolchain/i686-android-linux-4.4.3/bin/../lib/gcc/i686-android-linux/4.4.3/include -I/home/nlclient31/goldfish/goldfish/arch/x86/include -Iarch/x86/include/generated  -Iinclude -I/home/nlclient31/goldfish/goldfish/arch/x86/include/uapi -Iarch/x86/include/generated/uapi -I/home/nlclient31/goldfish/goldfish/include/uapi -Iinclude/generated/uapi -include /home/nlclient31/goldfish/goldfish/include/linux/kconfig.h -D__KERNEL__ -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -Werror-implicit-function-declaration -Wno-format-security -fno-delete-null-pointer-checks -Os -Wno-maybe-uninitialized -m32 -msoft-float -mregparm=3 -freg-struct-return -fno-pic -mpreferred-stack-boundary=2 -march=i686 -mtune=core2 -mtune=generic -Wa,-mtune=generic32 -ffreestanding -DCONFIG_AS_CFI=1 -DCONFIG_AS_CFI_SIGNAL_FRAME=1 -DCONFIG_AS_CFI_SECTIONS=1 -DCONFIG_AS_AVX=1 -pipe -Wno-sign-compare -fno-asynchronous-unwind-tables -mno-sse -mno-mmx -mno-sse2 -mno-3dnow -mno-avx -Wframe-larger-than=2048 -fno-stack-protector -fno-omit-frame-pointer -fno-optimize-sibling-calls -g -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fconserve-stack    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(pcspeaker)"  -D"KBUILD_MODNAME=KBUILD_STR(pcspeaker)" -c -o arch/x86/kernel/pcspeaker.o arch/x86/kernel/pcspeaker.c

source_arch/x86/kernel/pcspeaker.o := arch/x86/kernel/pcspeaker.c

deps_arch/x86/kernel/pcspeaker.o := \
  include/linux/platform_device.h \
    $(wildcard include/config/suspend.h) \
    $(wildcard include/config/hibernate/callbacks.h) \
    $(wildcard include/config/pm/sleep.h) \
  include/linux/device.h \
    $(wildcard include/config/debug/devres.h) \
    $(wildcard include/config/acpi.h) \
    $(wildcard include/config/pinctrl.h) \
    $(wildcard include/config/numa.h) \
    $(wildcard include/config/cma.h) \
    $(wildcard include/config/devtmpfs.h) \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/debug.h) \
    $(wildcard include/config/sysfs/deprecated.h) \
  include/linux/ioport.h \
    $(wildcard include/config/memory/hotremove.h) \
  include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
    $(wildcard include/config/kprobes.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  include/linux/compiler-gcc4.h \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  include/uapi/linux/types.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/types.h \
  /home/nlclient31/goldfish/goldfish/include/uapi/asm-generic/types.h \
  include/asm-generic/int-ll64.h \
  include/uapi/asm-generic/int-ll64.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/bitsperlong.h \
  include/asm-generic/bitsperlong.h \
  include/uapi/asm-generic/bitsperlong.h \
  /home/nlclient31/goldfish/goldfish/include/uapi/linux/posix_types.h \
  include/linux/stddef.h \
  include/uapi/linux/stddef.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/posix_types.h \
    $(wildcard include/config/x86/32.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/posix_types_32.h \
  /home/nlclient31/goldfish/goldfish/include/uapi/asm-generic/posix_types.h \
  include/linux/kobject.h \
  include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  include/linux/poison.h \
    $(wildcard include/config/illegal/pointer/value.h) \
  /home/nlclient31/goldfish/goldfish/include/uapi/linux/const.h \
  include/linux/sysfs.h \
    $(wildcard include/config/debug/lock/alloc.h) \
    $(wildcard include/config/sysfs.h) \
  include/linux/errno.h \
  include/uapi/linux/errno.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/errno.h \
  /home/nlclient31/goldfish/goldfish/include/uapi/asm-generic/errno.h \
  /home/nlclient31/goldfish/goldfish/include/uapi/asm-generic/errno-base.h \
  include/linux/lockdep.h \
    $(wildcard include/config/lockdep.h) \
    $(wildcard include/config/lock/stat.h) \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/prove/rcu.h) \
  include/linux/kobject_ns.h \
  include/linux/atomic.h \
    $(wildcard include/config/arch/has/atomic/or.h) \
    $(wildcard include/config/generic/atomic64.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/atomic.h \
    $(wildcard include/config/x86/64.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/processor.h \
    $(wildcard include/config/x86/vsmp.h) \
    $(wildcard include/config/smp.h) \
    $(wildcard include/config/cc/stackprotector.h) \
    $(wildcard include/config/paravirt.h) \
    $(wildcard include/config/m486.h) \
    $(wildcard include/config/x86/debugctlmsr.h) \
    $(wildcard include/config/xen.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/processor-flags.h \
    $(wildcard include/config/vm86.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/processor-flags.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/vm86.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/ptrace.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/segment.h \
    $(wildcard include/config/x86/32/lazy/gs.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/page_types.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/page_32_types.h \
    $(wildcard include/config/highmem4g.h) \
    $(wildcard include/config/highmem64g.h) \
    $(wildcard include/config/page/offset.h) \
    $(wildcard include/config/x86/pae.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/ptrace.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/ptrace-abi.h \
  include/linux/init.h \
    $(wildcard include/config/broken/rodata.h) \
    $(wildcard include/config/modules.h) \
  include/asm-generic/ptrace.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/vm86.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/math_emu.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/sigcontext.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/sigcontext.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/current.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/percpu.h \
    $(wildcard include/config/x86/64/smp.h) \
    $(wildcard include/config/x86/cmpxchg64.h) \
  include/linux/kernel.h \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/atomic/sleep.h) \
    $(wildcard include/config/ring/buffer.h) \
    $(wildcard include/config/tracing.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
  /home/nlclient31/goldfish/toolchain/prebuilt/linux-x86/toolchain/i686-android-linux-4.4.3/bin/../lib/gcc/i686-android-linux/4.4.3/include/stdarg.h \
  include/linux/linkage.h \
  include/linux/stringify.h \
  include/linux/export.h \
    $(wildcard include/config/have/underscore/symbol/prefix.h) \
    $(wildcard include/config/modversions.h) \
    $(wildcard include/config/unused/symbols.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/linkage.h \
    $(wildcard include/config/x86/alignment/16.h) \
  include/linux/bitops.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/bitops.h \
    $(wildcard include/config/x86/cmov.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/alternative.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/asm.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/cpufeature.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/required-features.h \
    $(wildcard include/config/x86/minimum/cpu/family.h) \
    $(wildcard include/config/math/emulation.h) \
    $(wildcard include/config/x86/use/3dnow.h) \
    $(wildcard include/config/x86/p6/nop.h) \
    $(wildcard include/config/matom.h) \
  include/asm-generic/bitops/fls64.h \
  include/asm-generic/bitops/find.h \
    $(wildcard include/config/generic/find/first/bit.h) \
  include/asm-generic/bitops/sched.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/arch_hweight.h \
  include/asm-generic/bitops/const_hweight.h \
  include/asm-generic/bitops/le.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/byteorder.h \
  include/linux/byteorder/little_endian.h \
  include/uapi/linux/byteorder/little_endian.h \
  include/linux/swab.h \
  include/uapi/linux/swab.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/swab.h \
  include/linux/byteorder/generic.h \
  include/asm-generic/bitops/ext2-atomic-setbit.h \
  include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  include/linux/typecheck.h \
  include/linux/printk.h \
    $(wildcard include/config/early/printk.h) \
  include/linux/kern_levels.h \
  include/linux/dynamic_debug.h \
  include/linux/string.h \
    $(wildcard include/config/binary/printf.h) \
  include/uapi/linux/string.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/string.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/string_32.h \
    $(wildcard include/config/kmemcheck.h) \
  include/uapi/linux/kernel.h \
  /home/nlclient31/goldfish/goldfish/include/uapi/linux/sysinfo.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/div64.h \
  include/asm-generic/percpu.h \
    $(wildcard include/config/debug/preempt.h) \
    $(wildcard include/config/have/setup/per/cpu/area.h) \
  include/linux/threads.h \
    $(wildcard include/config/nr/cpus.h) \
    $(wildcard include/config/base/small.h) \
  include/linux/percpu-defs.h \
    $(wildcard include/config/debug/force/weak/per/cpu.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/page.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/page_32.h \
    $(wildcard include/config/hugetlb/page.h) \
    $(wildcard include/config/debug/virtual.h) \
    $(wildcard include/config/flatmem.h) \
    $(wildcard include/config/x86/3dnow.h) \
  include/linux/range.h \
  include/asm-generic/memory_model.h \
    $(wildcard include/config/discontigmem.h) \
    $(wildcard include/config/sparsemem/vmemmap.h) \
    $(wildcard include/config/sparsemem.h) \
  include/asm-generic/getorder.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/pgtable_types.h \
    $(wildcard include/config/compat/vdso.h) \
    $(wildcard include/config/proc/fs.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/pgtable_32_types.h \
    $(wildcard include/config/highmem.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/pgtable-2level_types.h \
  include/asm-generic/pgtable-nopud.h \
  include/asm-generic/pgtable-nopmd.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/msr.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/msr.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/msr-index.h \
  /home/nlclient31/goldfish/goldfish/include/uapi/linux/ioctl.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/ioctl.h \
  include/asm-generic/ioctl.h \
  include/uapi/asm-generic/ioctl.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/cpumask.h \
  include/linux/cpumask.h \
    $(wildcard include/config/cpumask/offstack.h) \
    $(wildcard include/config/hotplug/cpu.h) \
    $(wildcard include/config/debug/per/cpu/maps.h) \
    $(wildcard include/config/disable/obsolete/cpumask/functions.h) \
  include/linux/bitmap.h \
  include/linux/bug.h \
    $(wildcard include/config/generic/bug.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/debug/bugverbose.h) \
  include/asm-generic/bug.h \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/desc_defs.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/nops.h \
    $(wildcard include/config/mk7.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/special_insns.h \
  include/linux/personality.h \
  include/uapi/linux/personality.h \
  include/linux/cache.h \
    $(wildcard include/config/arch/has/cache/line/size.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/cache.h \
    $(wildcard include/config/x86/l1/cache/shift.h) \
    $(wildcard include/config/x86/internode/cache/shift.h) \
  include/linux/math64.h \
  include/linux/err.h \
  include/linux/irqflags.h \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/irqflags.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/cmpxchg.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/cmpxchg_32.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/atomic64_32.h \
  include/asm-generic/atomic-long.h \
  include/linux/spinlock.h \
    $(wildcard include/config/debug/spinlock.h) \
    $(wildcard include/config/generic/lockbreak.h) \
    $(wildcard include/config/preempt.h) \
  include/linux/preempt.h \
    $(wildcard include/config/context/tracking.h) \
    $(wildcard include/config/preempt/count.h) \
    $(wildcard include/config/preempt/notifiers.h) \
  include/linux/thread_info.h \
    $(wildcard include/config/compat.h) \
    $(wildcard include/config/debug/stack/usage.h) \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/thread_info.h \
    $(wildcard include/config/ia32/emulation.h) \
  include/linux/bottom_half.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/barrier.h \
    $(wildcard include/config/x86/ppro/fence.h) \
    $(wildcard include/config/x86/oostore.h) \
  include/linux/spinlock_types.h \
  include/linux/spinlock_types_up.h \
  include/linux/rwlock_types.h \
  include/linux/spinlock_up.h \
  include/linux/rwlock.h \
  include/linux/spinlock_api_up.h \
  include/linux/kref.h \
  include/linux/mutex.h \
    $(wildcard include/config/debug/mutexes.h) \
    $(wildcard include/config/mutex/spin/on/owner.h) \
    $(wildcard include/config/have/arch/mutex/cpu/relax.h) \
  include/linux/wait.h \
  include/uapi/linux/wait.h \
  include/linux/klist.h \
  include/linux/pinctrl/devinfo.h \
  include/linux/pm.h \
    $(wildcard include/config/vt/console/sleep.h) \
    $(wildcard include/config/pm.h) \
    $(wildcard include/config/pm/runtime.h) \
    $(wildcard include/config/pm/clk.h) \
    $(wildcard include/config/pm/generic/domains.h) \
  include/linux/workqueue.h \
    $(wildcard include/config/debug/objects/work.h) \
    $(wildcard include/config/freezer.h) \
  include/linux/timer.h \
    $(wildcard include/config/timer/stats.h) \
    $(wildcard include/config/debug/objects/timers.h) \
  include/linux/ktime.h \
    $(wildcard include/config/ktime/scalar.h) \
  include/linux/time.h \
    $(wildcard include/config/arch/uses/gettimeoffset.h) \
  include/linux/seqlock.h \
  include/uapi/linux/time.h \
  include/linux/jiffies.h \
  include/linux/timex.h \
  include/uapi/linux/timex.h \
  /home/nlclient31/goldfish/goldfish/include/uapi/linux/param.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/uapi/asm/param.h \
  include/asm-generic/param.h \
    $(wildcard include/config/hz.h) \
  include/uapi/asm-generic/param.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/timex.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/tsc.h \
    $(wildcard include/config/x86/tsc.h) \
  include/linux/debugobjects.h \
    $(wildcard include/config/debug/objects.h) \
    $(wildcard include/config/debug/objects/free.h) \
  include/linux/completion.h \
  include/linux/ratelimit.h \
  include/linux/uidgid.h \
    $(wildcard include/config/uidgid/strict/type/checks.h) \
    $(wildcard include/config/user/ns.h) \
  include/linux/highuid.h \
  /home/nlclient31/goldfish/goldfish/arch/x86/include/asm/device.h \
    $(wildcard include/config/x86/dev/dma/ops.h) \
    $(wildcard include/config/intel/iommu.h) \
    $(wildcard include/config/amd/iommu.h) \
  include/linux/pm_wakeup.h \
  include/linux/mod_devicetable.h \
  include/linux/uuid.h \
  include/uapi/linux/uuid.h \

arch/x86/kernel/pcspeaker.o: $(deps_arch/x86/kernel/pcspeaker.o)

$(deps_arch/x86/kernel/pcspeaker.o):
