	.file	"bounds.c"
# GNU C (GCC) version 4.4.3 (i686-android-linux)
#	compiled by GNU C version 4.4.3, GMP version 4.2.4, MPFR version 2.4.1.
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -nostdinc
# -I/home/nlclient31/goldfish/goldfish/arch/x86/include
# -Iarch/x86/include/generated -Iinclude
# -I/home/nlclient31/goldfish/goldfish/arch/x86/include/uapi
# -Iarch/x86/include/generated/uapi
# -I/home/nlclient31/goldfish/goldfish/include/uapi
# -Iinclude/generated/uapi -iprefix
# /home/nlclient31/goldfish/toolchain/prebuilt/linux-x86/toolchain/i686-android-linux-4.4.3/bin/../lib/gcc/i686-android-linux/4.4.3/
# -D__KERNEL__ -DCONFIG_AS_CFI=1 -DCONFIG_AS_CFI_SIGNAL_FRAME=1
# -DCONFIG_AS_CFI_SECTIONS=1 -DCONFIG_AS_AVX=1 -DKBUILD_STR(s)=#s
# -DKBUILD_BASENAME=KBUILD_STR(bounds) -DKBUILD_MODNAME=KBUILD_STR(bounds)
# -isystem
# /home/nlclient31/goldfish/toolchain/prebuilt/linux-x86/toolchain/i686-android-linux-4.4.3/bin/../lib/gcc/i686-android-linux/4.4.3/include
# -include /home/nlclient31/goldfish/goldfish/include/linux/kconfig.h -MD
# kernel/.bounds.s.d kernel/bounds.c -mno-android -m32 -msoft-float
# -mregparm=3 -mpreferred-stack-boundary=2 -march=i686 -mtune=core2
# -mtune=generic -mno-sse -mno-mmx -mno-sse2 -mno-3dnow -mno-avx
# -auxbase-strip kernel/bounds.s -g -Os -Wall -Wundef -Wstrict-prototypes
# -Wno-trigraphs -Werror-implicit-function-declaration -Wno-format-security
# -Wno-maybe-uninitialized -Wno-sign-compare -Wframe-larger-than=2048
# -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-aliasing
# -fno-common -fno-delete-null-pointer-checks -freg-struct-return -fno-pic
# -ffreestanding -fno-asynchronous-unwind-tables -fno-stack-protector
# -fno-omit-frame-pointer -fno-optimize-sibling-calls -fno-strict-overflow
# -fconserve-stack -fverbose-asm
# options enabled:  -falign-loops -fargument-alias -fauto-inc-dec
# -fbranch-count-reg -fcaller-saves -fcprop-registers -fcrossjumping
# -fcse-follow-jumps -fdefer-pop -fdwarf2-cfi-asm -fearly-inlining
# -feliminate-unused-debug-types -fexpensive-optimizations
# -fforward-propagate -ffunction-cse -fgcse -fgcse-lm
# -fguess-branch-probability -fident -fif-conversion -fif-conversion2
# -findirect-inlining -finline -finline-functions
# -finline-functions-called-once -finline-small-functions -fipa-cp
# -fipa-pure-const -fipa-reference -fira-share-save-slots
# -fira-share-spill-slots -fivopts -fkeep-static-consts
# -fleading-underscore -fmath-errno -fmerge-constants -fmerge-debug-strings
# -fmove-loop-invariants -foptimize-register-move -fpeephole -fpeephole2
# -fpessimistic-inline-stack-limit -freg-struct-return -fregmove
# -freorder-blocks -freorder-functions -frerun-cse-after-loop
# -fsched-interblock -fsched-spec -fsched-stalled-insns-dep
# -fschedule-insns2 -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fstrict-enum-precision -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-builtin-call-dce -ftree-ccp
# -ftree-ch -ftree-copy-prop -ftree-copyrename -ftree-cselim -ftree-dce
# -ftree-dominator-opts -ftree-dse -ftree-fre -ftree-loop-im
# -ftree-loop-ivcanon -ftree-loop-optimize -ftree-lr-shrinking
# -ftree-parallelize-loops= -ftree-pre -ftree-reassoc -ftree-scev-cprop
# -ftree-sink -ftree-sra -ftree-switch-conversion -ftree-ter
# -ftree-vect-loop-version -ftree-vrp -funit-at-a-time -fvar-tracking
# -fvect-cost-model -fverbose-asm -fzero-initialized-in-bss -m32
# -m96bit-long-double -malign-stringops -mbionic -mfused-madd -mieee-fp
# -mno-fancy-math-387 -mno-red-zone -mno-sse4 -mpush-args -msahf
# -mtls-direct-seg-refs

	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.text
.Ltext0:
# Compiler executable checksum: 64a0ebf9b5ef7356ec682eb05828d2ff

	.cfi_sections	.debug_frame
.globl foo
	.type	foo, @function
foo:
.LFB132:
	.file 1 "kernel/bounds.c"
	.loc 1 15 0
	.cfi_startproc
	pushl	%ebp	#
.LCFI0:
	.cfi_def_cfa_offset 8
	movl	%esp, %ebp	#,
	.cfi_offset 5, -8
.LCFI1:
	.cfi_def_cfa_register 5
	.loc 1 17 0
#APP
# 17 "kernel/bounds.c" 1
	
->NR_PAGEFLAGS $23 __NR_PAGEFLAGS	#
# 0 "" 2
	.loc 1 18 0
# 18 "kernel/bounds.c" 1
	
->MAX_NR_ZONES $3 __MAX_NR_ZONES	#
# 0 "" 2
	.loc 1 19 0
# 19 "kernel/bounds.c" 1
	
->NR_PCG_FLAGS $3 __NR_PCG_FLAGS	#
# 0 "" 2
	.loc 1 21 0
#NO_APP
	popl	%ebp	#
	.cfi_restore 5
.LCFI2:
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE132:
	.size	foo, .-foo
.Letext0:
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.long	.LFB132-.Ltext0
	.long	.LCFI0-.Ltext0
	.value	0x2
	.byte	0x74
	.sleb128 4
	.long	.LCFI0-.Ltext0
	.long	.LCFI1-.Ltext0
	.value	0x2
	.byte	0x74
	.sleb128 8
	.long	.LCFI1-.Ltext0
	.long	.LCFI2-.Ltext0
	.value	0x2
	.byte	0x75
	.sleb128 8
	.long	.LCFI2-.Ltext0
	.long	.LFE132-.Ltext0
	.value	0x2
	.byte	0x74
	.sleb128 4
	.long	0x0
	.long	0x0
	.file 2 "include/linux/page-flags.h"
	.file 3 "include/linux/mmzone.h"
	.file 4 "include/linux/printk.h"
	.file 5 "include/linux/kernel.h"
	.file 6 "include/linux/page_cgroup.h"
	.section	.debug_info
	.long	0x1c6
	.value	0x2
	.long	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.long	.LASF52
	.byte	0x1
	.long	.LASF53
	.long	.LASF54
	.long	.Ltext0
	.long	.Letext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF3
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF4
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF6
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.long	.LASF9
	.uleb128 0x2
	.byte	0x1
	.byte	0x2
	.long	.LASF10
	.uleb128 0x5
	.long	.LASF40
	.byte	0x4
	.byte	0x2
	.byte	0x4a
	.long	0x137
	.uleb128 0x6
	.long	.LASF11
	.sleb128 0
	.uleb128 0x6
	.long	.LASF12
	.sleb128 1
	.uleb128 0x6
	.long	.LASF13
	.sleb128 2
	.uleb128 0x6
	.long	.LASF14
	.sleb128 3
	.uleb128 0x6
	.long	.LASF15
	.sleb128 4
	.uleb128 0x6
	.long	.LASF16
	.sleb128 5
	.uleb128 0x6
	.long	.LASF17
	.sleb128 6
	.uleb128 0x6
	.long	.LASF18
	.sleb128 7
	.uleb128 0x6
	.long	.LASF19
	.sleb128 8
	.uleb128 0x6
	.long	.LASF20
	.sleb128 9
	.uleb128 0x6
	.long	.LASF21
	.sleb128 10
	.uleb128 0x6
	.long	.LASF22
	.sleb128 11
	.uleb128 0x6
	.long	.LASF23
	.sleb128 12
	.uleb128 0x6
	.long	.LASF24
	.sleb128 13
	.uleb128 0x6
	.long	.LASF25
	.sleb128 14
	.uleb128 0x6
	.long	.LASF26
	.sleb128 15
	.uleb128 0x6
	.long	.LASF27
	.sleb128 16
	.uleb128 0x6
	.long	.LASF28
	.sleb128 17
	.uleb128 0x6
	.long	.LASF29
	.sleb128 18
	.uleb128 0x6
	.long	.LASF30
	.sleb128 19
	.uleb128 0x6
	.long	.LASF31
	.sleb128 20
	.uleb128 0x6
	.long	.LASF32
	.sleb128 21
	.uleb128 0x6
	.long	.LASF33
	.sleb128 22
	.uleb128 0x6
	.long	.LASF34
	.sleb128 23
	.uleb128 0x6
	.long	.LASF35
	.sleb128 8
	.uleb128 0x6
	.long	.LASF36
	.sleb128 12
	.uleb128 0x6
	.long	.LASF37
	.sleb128 8
	.uleb128 0x6
	.long	.LASF38
	.sleb128 4
	.uleb128 0x6
	.long	.LASF39
	.sleb128 11
	.byte	0x0
	.uleb128 0x7
	.long	.LASF41
	.byte	0x4
	.byte	0x3
	.value	0x104
	.long	0x15d
	.uleb128 0x6
	.long	.LASF42
	.sleb128 0
	.uleb128 0x6
	.long	.LASF43
	.sleb128 1
	.uleb128 0x6
	.long	.LASF44
	.sleb128 2
	.uleb128 0x6
	.long	.LASF45
	.sleb128 3
	.byte	0x0
	.uleb128 0x8
	.byte	0x4
	.byte	0x6
	.byte	0x4
	.long	0x17e
	.uleb128 0x6
	.long	.LASF46
	.sleb128 0
	.uleb128 0x6
	.long	.LASF47
	.sleb128 1
	.uleb128 0x6
	.long	.LASF48
	.sleb128 2
	.uleb128 0x6
	.long	.LASF49
	.sleb128 3
	.byte	0x0
	.uleb128 0x9
	.byte	0x1
	.string	"foo"
	.byte	0x1
	.byte	0xe
	.byte	0x1
	.long	.LFB132
	.long	.LFE132
	.long	.LLST0
	.uleb128 0xa
	.long	0x41
	.long	0x19e
	.uleb128 0xb
	.byte	0x0
	.uleb128 0xc
	.long	.LASF50
	.byte	0x4
	.byte	0x24
	.long	0x193
	.byte	0x1
	.byte	0x1
	.uleb128 0xa
	.long	0x67
	.long	0x1b6
	.uleb128 0xb
	.byte	0x0
	.uleb128 0xd
	.long	.LASF51
	.byte	0x5
	.value	0x1b2
	.long	0x1c4
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.long	0x1ab
	.byte	0x0
	.section	.debug_abbrev
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0x0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0x0
	.byte	0x0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0x0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0x0
	.byte	0x0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0x0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.byte	0x0
	.byte	0x0
	.uleb128 0x5
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0x6
	.uleb128 0x28
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0x0
	.byte	0x0
	.uleb128 0x7
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0x8
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0x0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0x0
	.byte	0x0
	.byte	0x0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0x0
	.byte	0x0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0x0
	.byte	0x0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0x0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.byte	0x0
	.section	.debug_pubnames,"",@progbits
	.long	0x16
	.value	0x2
	.long	.Ldebug_info0
	.long	0x1ca
	.long	0x17e
	.string	"foo"
	.long	0x0
	.section	.debug_aranges,"",@progbits
	.long	0x1c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x4
	.byte	0x0
	.value	0x0
	.value	0x0
	.long	.Ltext0
	.long	.Letext0-.Ltext0
	.long	0x0
	.long	0x0
	.section	.debug_str,"MS",@progbits,1
.LASF48:
	.string	"PCG_MIGRATION"
.LASF25:
	.string	"PG_head"
.LASF50:
	.string	"console_printk"
.LASF21:
	.string	"PG_reserved"
.LASF43:
	.string	"ZONE_NORMAL"
.LASF28:
	.string	"PG_mappedtodisk"
.LASF39:
	.string	"PG_slob_free"
.LASF11:
	.string	"PG_locked"
.LASF15:
	.string	"PG_dirty"
.LASF24:
	.string	"PG_writeback"
.LASF22:
	.string	"PG_private"
.LASF34:
	.string	"__NR_PAGEFLAGS"
.LASF33:
	.string	"PG_uncached"
.LASF45:
	.string	"__MAX_NR_ZONES"
.LASF36:
	.string	"PG_fscache"
.LASF13:
	.string	"PG_referenced"
.LASF27:
	.string	"PG_swapcache"
.LASF41:
	.string	"zone_type"
.LASF7:
	.string	"long unsigned int"
.LASF3:
	.string	"short unsigned int"
.LASF30:
	.string	"PG_swapbacked"
.LASF35:
	.string	"PG_checked"
.LASF1:
	.string	"unsigned char"
.LASF53:
	.string	"kernel/bounds.c"
.LASF26:
	.string	"PG_tail"
.LASF40:
	.string	"pageflags"
.LASF46:
	.string	"PCG_LOCK"
.LASF4:
	.string	"unsigned int"
.LASF12:
	.string	"PG_error"
.LASF6:
	.string	"long long unsigned int"
.LASF54:
	.string	"/home/nlclient31/goldfish/goldfish"
.LASF37:
	.string	"PG_pinned"
.LASF18:
	.string	"PG_slab"
.LASF19:
	.string	"PG_owner_priv_1"
.LASF17:
	.string	"PG_active"
.LASF23:
	.string	"PG_private_2"
.LASF5:
	.string	"long long int"
.LASF16:
	.string	"PG_lru"
.LASF52:
	.string	"GNU C 4.4.3"
.LASF8:
	.string	"char"
.LASF31:
	.string	"PG_unevictable"
.LASF38:
	.string	"PG_savepinned"
.LASF2:
	.string	"short int"
.LASF51:
	.string	"hex_asc"
.LASF47:
	.string	"PCG_USED"
.LASF20:
	.string	"PG_arch_1"
.LASF9:
	.string	"long int"
.LASF44:
	.string	"ZONE_MOVABLE"
.LASF14:
	.string	"PG_uptodate"
.LASF49:
	.string	"__NR_PCG_FLAGS"
.LASF0:
	.string	"signed char"
.LASF29:
	.string	"PG_reclaim"
.LASF10:
	.string	"_Bool"
.LASF32:
	.string	"PG_mlocked"
.LASF42:
	.string	"ZONE_DMA"
	.ident	"GCC: (GNU) 4.4.3"
	.section	.note.GNU-stack,"",@progbits
